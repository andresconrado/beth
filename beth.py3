#!/usr/bin/python3

import os, random, string, nltk, math, shutil, subprocess

from random           import choice,randint
from collections      import deque
from tl.rename.case   import transform_sentence_case
from wand.image       import Image
from wand.display     import display
from wand.font        import Font
from wand.color       import Color
from datetime         import datetime, date, time
from progress.bar     import Bar
from progress.spinner import Spinner


# Separador
def sep():
  term = shutil.get_terminal_size()
  print('-' * term[0])

# limpiar terminal
os.system('cls' if os.name == 'nt' else 'clear')

sep()
print('[BETH 0.1] :: DATA.GMTRH')
sep()


##########################
# PROCESAR TEXTO
##########################
# funcion para generar el texto aleatorio, 
# y añadirlo al archivo 'cadaveres.txt'

def exquisiteCadaver(m):
    
    global texto

    words_found  = set()
    words_print  = deque(maxlen=m)
    dictionaries = ['dic_es.txt','dic_en.txt']
    dictionary   = open(random.choice(dictionaries)).read().split()
    lines        = sum(1 for line in dictionary)

    random.shuffle(dictionary)

    bardic = Bar('[TMRH]: ', max=lines, bar_prefix = ' [', bar_suffix = '] ')
    
    for word in dictionary: 
      words_found.add(word)
      bardic.next()
    bardic.finish()

    barwrd = Spinner('[GMTRH]: ')

    for w in words_found:
      probe = randint(1,m)
      if   probe == 1 or probe == 2 or probe == 3 or probe == 4:
        words_print.append(w)
      elif probe == 5 or probe == 6 or probe == 7:
        words_print.append(w + '.')
      elif probe == 8:
        words_print.append(w + ';')
      else:
        words_print.append(w + ',')
      barwrd.next()
    barwrd.finish()

    sentences      = ' '.join(str(s) for s in words_print).strip()
    sent_tokenizer = nltk.data.load('tokenizers/punkt/spanish.pickle')
    cadaver        = sent_tokenizer.tokenize(sentences)
    cadaver        = [sent.capitalize() for sent in cadaver]
    cadaver        = ' '.join(cadaver)

    with open('cadaveres.txt', 'w') as f:
      if cadaver.endswith('.'):
        texto = cadaver
        print(texto, file = f)
      elif cadaver.endswith(',') or cadaver.endswith(';') or cadaver.endswith(':'):
        texto = cadaver[:-1] + '.'
        print(texto, file = f)
      else:
        texto = cadaver + '.'
        print(texto, file = f)

# Ejecutar 
exquisiteCadaver(9)


##########################
# PROCESAR IMGEN
##########################
# funcion para tomar imagen aleatoria del directorio 'img-base', 
# escribir el texto sobre ella y con una fuente determinada
# y guardarlo en el directorio 'img-final'

def exquisiteImage(c,f,o,d):
    color   = Color(random.choice(c))
    ttf     = random.choice(os.listdir(f))
    origen  = random.choice(os.listdir(o))
    destiny = d
            
    with Image(filename = o + origen) as img:
      iwidth  = img.width
      iheight = img.height
      pad     = iwidth // 9
      padd    = pad * 2
      boxh    = iwidth - padd 
      boxv    = iheight - padd
      fntsize = (pad // 9) * 5 
      ifont   = Font(path = f + ttf, size = fntsize, color = color)
      g       = random.choice(['east','west'])

      with img.clone() as i:
        bn = os.path.splitext(origen)[0]
        i.caption(texto, gravity = g, width = boxh, font = ifont, left = pad)
        i.format = 'jpg'
        randstr = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(9))
        objeto = str(destiny + bn + '-' + randstr + '{0:-%m%d%Y-%H%M%S}.jpg'.format(datetime.now()))
        i.save(filename = objeto)
        print('\n[SHKNH]: ' + objeto)
        sep()
        print('[DATA.GMTRH.OVER: STATUS:OK]\n[SHKNH.GLITCH.START]:')
        # Glitching 
        subprocess.call("./glitch.py2 --file " + objeto, shell=True)
        sep()
        

# Ejecutar
exquisiteImage([
  '#00ffff',
  '#ff00ff',
  '#00ff00'],
  'fonts/',
  'img-base/',
  'img-final/')
